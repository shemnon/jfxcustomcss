package org.bitbucket.shemnon.flipper;

import javafx.animation.*;
import javafx.beans.property.DoubleProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.collections.ObservableList;
import javafx.geometry.Pos;
import javafx.scene.control.Label;
import javafx.scene.effect.PerspectiveTransform;
import javafx.scene.layout.Region;
import javafx.scene.layout.StackPane;
import javafx.util.Duration;

public class FlipPane extends StackPane {

    ObjectProperty<Duration> flipDuration = new SimpleObjectProperty<>(Duration.millis(500));
    DoubleProperty currentAngle = new SimpleDoubleProperty(90);
    DoubleProperty flipAngle = new SimpleDoubleProperty(90);
    ObjectProperty<Region> front = new SimpleObjectProperty<>();
    ObjectProperty<Region> back = new SimpleObjectProperty<>();

    protected PerspectiveTransform transform = new PerspectiveTransform();

    protected Timeline flipTimeline = new Timeline();
    
    public FlipPane() {
        this(new Label("Front"), new Label("Back"));
    }

    public FlipPane(Region front, Region back) {
        super(front, back);
        
        this.front.set(front);
        this.back.set(back);

        setAlignment(Pos.CENTER);

        front.setEffect(transform);
        back.setEffect(transform);
        back.setVisible(false);
        setupTransform();
        currentAngle.addListener(vm -> updateAngle());
        

        layoutBoundsProperty().addListener(observable -> setupTransform());
        front.layoutBoundsProperty().addListener(observable -> setupTransform());
        back.layoutBoundsProperty().addListener(observable -> setupTransform());
        getStyleClass().setAll("flipper");
    }

    public double getCurrentAngle() {
        return currentAngle.get();
    }

    public DoubleProperty currentAngleProperty() {
        return currentAngle;
    }

    public void setCurrentAngle(double currentAngle) {
        currentAngle = currentAngle % 360;
        if (currentAngle > 0) {
            currentAngle += 360;
        }
        this.currentAngle.set(currentAngle);
        updateAngle();
    }

    public Region getBack() {
        return back.get();
    }

    public ObjectProperty<Region> backProperty() {
        return back;
    }

    public void setBack(Region back) {
        this.back.set(back);
    }

    public Double getFlipAngle() {
        return flipAngle.get();
    }

    public DoubleProperty flipAngleProperty() {
        return flipAngle;
    }

    public void setFlipAngle(Double flipAngle) {
        this.flipAngle.set(flipAngle);
    }

    public Duration getFlipDuration() {
        return flipDuration.get();
    }

    public ObjectProperty<Duration> flipDurationProperty() {
        return flipDuration;
    }

    public void setFlipDuration(Duration flipDuration) {
        this.flipDuration.set(flipDuration);
    }

    public Region getFront() {
        return front.get();
    }

    public ObjectProperty<Region> frontProperty() {
        return front;
    }

    public void setFront(Region front) {
        this.front.set(front);
    }

    public boolean isFrontVisible() {
        // presume angle in [0...360)
        return getCurrentAngle() < 180;
    }
    
    public boolean isBackVisible() {
        return !isFrontVisible();
    }


    private void setupTransform() {
        getLayoutBounds();  // has side effect of setting width and height, even to child elements
        updateAngle();
    }

    private void updateAngle() {
        // calculate new transform
        double effectiveAngle = currentAngle.get() % 180;
        Region target = isFrontVisible() ? front.get() : back.get();

        front.get().setVisible(isFrontVisible());
        back.get().setVisible(!isFrontVisible());

        double radiusH = target.getWidth() / 2;
        double back = target.getWidth() / 10;
        double lx = (radiusH - Math.sin(Math.toRadians(effectiveAngle)) * radiusH);
        double rx = (radiusH + Math.sin(Math.toRadians(effectiveAngle)) * radiusH);
        double uly = (-Math.cos(Math.toRadians(effectiveAngle)) * back);
        double ury = -uly;
        transform.setUlx(lx);
        transform.setUly(uly);
        transform.setUrx(rx);
        transform.setUry(ury);
        transform.setLrx(rx);
        transform.setLry(target.getHeight() + uly);
        transform.setLlx(lx);
        transform.setLly(target.getHeight() + ury);
    }

    public void flip() {
        if (flipTimeline.getStatus() != Animation.Status.STOPPED) return;
        
        final ObservableList<KeyFrame> keyFrames = flipTimeline.getKeyFrames();

        double startAngle = currentAngle.get();
        double finalAngle;
        double flipAt;
        Region first;
        Region second;
        
        if (startAngle < 180) {
            finalAngle = flipAngle.get() + 180;
            flipAt = 1 - (startAngle / 180);
            first = front.get();
            second = back.get();
        } else {
            finalAngle = flipAngle.get();
            flipAt = finalAngle / 180;
            first = back.get();
            second = front.get();
        }
        

        Duration totalDuration = flipDuration.get();
        Duration flipPoint = totalDuration.multiply(flipAt);
        
        
        keyFrames.setAll(
                new KeyFrame(Duration.ZERO,
                        new KeyValue(currentAngle, currentAngle.get()),
                        new KeyValue(first.visibleProperty(), true),
                        new KeyValue(second.visibleProperty(), false)),
                new KeyFrame(flipPoint,
                        new KeyValue(currentAngle, 180, Interpolator.EASE_IN),
                        new KeyValue(first.visibleProperty(), true),
                        new KeyValue(second.visibleProperty(), false)),
                new KeyFrame(flipPoint,
                        new KeyValue(currentAngle, 180),
                        new KeyValue(first.visibleProperty(), false),
                        new KeyValue(second.visibleProperty(), true)),
                new KeyFrame(totalDuration,
                        new KeyValue(currentAngle, finalAngle, Interpolator.EASE_OUT),
                        new KeyValue(first.visibleProperty(), false),
                        new KeyValue(second.visibleProperty(), true))
        );
        flipTimeline.play();
    }
}
