package org.bitbucket.shemnon.flipper;

import javafx.application.Application;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;


public class Demo extends Application {

    @Override
    public void start(Stage stage) throws Exception {

        ImageView scientistImage = new ImageView("/org/bitbucket/shemnon/flipper/JavaDukeFY14_Scientist_full_small.png");
        scientistImage.setFitHeight(400);
        scientistImage.setPreserveRatio(true);
        scientistImage.setSmooth(true);
        scientistImage.setCache(true);


        VBox front = new VBox(
                new Label("Front"),
                scientistImage,
                new Label("Footer")
        );
        front.getStyleClass().add("front");
        front.setAlignment(Pos.CENTER);
        front.setPadding(new Insets(20, 20, 20, 20));
        front.setMaxSize(500, 500);

        ImageView rockerImage = new ImageView("/org/bitbucket/shemnon/flipper/702px-Duke-Guitar.png");
        rockerImage.setFitHeight(400);
        rockerImage.setPreserveRatio(true);
        rockerImage.setSmooth(true);
        rockerImage.setCache(true);

        VBox back = new VBox(
                new Label("Back"),
                rockerImage,
                new Label("Footer")
        );
        back.getStyleClass().add("back");
        back.setAlignment(Pos.CENTER);
        back.setPadding(new Insets(20, 20, 20, 20));
        back.setMaxSize(500, 500);


        FlipPane flipper = new FlipPane(front, back);
        VBox.setVgrow(flipper, Priority.ALWAYS);
        flipper.setId("theFlipper");
        flipper.setPadding(new Insets(20, 20, 20, 20));

        Slider angleSlider = new Slider(0, 360, 90);
        angleSlider.setMaxWidth(5000);
        flipper.currentAngle.bindBidirectional(angleSlider.valueProperty());

        Button flipButton = new Button("Flip!");
        flipButton.setOnAction(EventHandler -> flipper.flip());
        flipButton.setMaxWidth(5000);

        VBox vbox  = new VBox(10, flipper, angleSlider, flipButton);
        vbox.setPadding(new Insets(50, 50, 50, 50));

        Scene aScene = new Scene(vbox);
        aScene.getStylesheets().add("/org/bitbucket/shemnon/flipper/Demo.css");

        stage.setScene(aScene);
        stage.show();
    }
}
